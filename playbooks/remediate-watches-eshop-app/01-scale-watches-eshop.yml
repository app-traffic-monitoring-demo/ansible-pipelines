---
- name: Scale Watches-eShop application
  hosts: localhost
  gather_facts: false

  tasks:
    - name: Set AAP job id
      ansible.builtin.set_stats:
        data:
          aap_job_id: "{{ tower_job_id }}"

    - name: Convert Dynatrace alert tags to list
      ansible.builtin.set_fact:
        dynatrace_json_tags: "{{ dynatrace_alert_tags | split(',') }}"

    - name: Get namespace from list
      ansible.builtin.set_fact:
        dynatrace_tag_namespace: "{{ item.split(':')[1] }}"
      loop: "{{ dynatrace_json_tags }}"
      when: "'environment' in item"

    - name: Get app from list
      ansible.builtin.set_fact:
        dynatrace_tag_app: "{{ item.split(':')[1] }}"
      loop: "{{ dynatrace_json_tags }}"
      when: "'app' in item"

    - name: Clone Watches-eShop repository
      ansible.builtin.git:
        repo: "http://gitea:openshift@gitea.{{ ocp_ingress_domain }}/gitea/watches-eshop.git"
        dest: /tmp/watches-eshop
        version: master

    - name: "Read Watches-eShop {{ dynatrace_tag_app }} deployment file"
      ansible.builtin.include_vars:
        file: "/tmp/watches-eshop/deploy/{{ dynatrace_tag_app }}/base/deployment.yaml"
        name: deployment

    - name: Set current_replicas
      ansible.builtin.set_fact:
        current_replicas: "{{ deployment.spec.replicas }}"
   
    - name: "Scale Watches-eShop {{ dynatrace_tag_app }} deployment"
      block:
        - name: "Change Watches-eShop {{ dynatrace_tag_app }} deployment replicas"
          ansible.builtin.replace:
            path: "/tmp/watches-eshop/deploy/{{ dynatrace_tag_app }}/base/deployment.yaml"
            regexp: 'replicas: {{ current_replicas }}'
            replace: "replicas: {{ new_replicas }}"

        - name: Git Push to master
          ansible.builtin.shell: |
            git config user.email gitea@gitea.com
            git config user.name gitea
            git add .
            git commit -m "Scaling replicas watches-eshop {{ dynatrace_tag_app }} using Ansible"
            git push
          args:
            chdir: /tmp/watches-eshop/

        - name: Get ArgoCD bearer token
          ansible.builtin.uri:
            url: "https://argocd-server-{{ ocp_namespace }}.{{ ocp_ingress_domain }}/api/v1/session"
            method: POST
            validate_certs: false
            headers:
              Content-Type: application/json
            body_format: json
            body:
              username: admin
              password: redhat
          register: argocd_token

        - name: "Refresh ArgoCD application {{ dynatrace_tag_app }}"
          ansible.builtin.uri:
            url: "https://argocd-server-{{ ocp_namespace }}.{{ ocp_ingress_domain }}/api/v1/applications/{{ dynatrace_tag_app }}?refresh=true"
            method: GET
            return_content: true
            validate_certs: false
            headers:
              Content-Type: application/json
              Authorization: "Bearer {{ argocd_token.json.token }}"

        - name: "Check sync ArgoCD application {{ dynatrace_tag_app }}"
          ansible.builtin.uri:
            url: "https://argocd-server-{{ ocp_namespace }}.{{ ocp_ingress_domain }}/api/v1/applications/{{ dynatrace_tag_app }}"
            method: GET
            return_content: true
            validate_certs: false
            headers:
              Content-Type: application/json
              Authorization: "Bearer {{ argocd_token.json.token }}"
          register: argocd_sync_status
          until: argocd_sync_status.json.status.health.status == "Healthy" and argocd_sync_status.json.operation is undefined

        - name: "Wait till Watches-eShop {{ dynatrace_tag_app }} deployment is ready"
          kubernetes.core.k8s_info:
            api_version: apps/v1
            kind: Deployment
            name: "{{ dynatrace_tag_app }}"
            namespace: "{{ dynatrace_tag_namespace }}"
            wait: yes
            wait_sleep: 10
            wait_timeout: 360
          register: ocp_app_deployment_obj
          until:
           - ocp_app_deployment_obj.resources[0].status.readyReplicas is defined
           - "ocp_app_deployment_obj.resources[0].status.replicas == \
               ocp_app_deployment_obj.resources[0].status.readyReplicas"
          retries: 12
          delay: 10 
      when: current_replicas != new_replicas and new_replicas >= 1
